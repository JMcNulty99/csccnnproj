// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "CoreMinimal.h"
#include "Neural_Network.h"
#include "Runtime/Engine/Classes/Engine/TextureRenderTarget2D.h"

struct result2
{
	std::vector<std::vector<float>>x;
	std::vector<std::vector<float>>y;
};

struct result3
{
	_math::mat3 x;
	_math::mat3 y;
};

struct result4
{
	_math::mat3* x;
	_math::mat3 y;
};


/**
 * 
 */
class CSC546_API Conv_Net
{
	std::vector<float> fully_conn_mult(std::vector<float>,std::vector<std::vector<float>>);
	
	_math::mat3 forward_con(_math::mat3, std::vector<_math::mat3>, int);
	
	result2 fully_con_back(std::vector<float>, std::vector<std::vector<float>>&, std::vector<float>&);
	
	result4 back_con(_math::mat3, std::vector<_math::mat3>, _math::mat3, int);
		
	result3 forward_max_pool(_math::mat3 input, int step);
	
	_math::mat3 backwards_max_pool(_math::mat3 loc, int step, _math::mat3 err);

	void init();

	Neural_Network* layer_5 = new Neural_Network({ 49 * 3, 15, 3, 3, 4 });

public:
	std::vector<float> train(_math::mat3 img, int label);
	std::vector<float> get_output(_math::mat3 img);
	Conv_Net();
	~Conv_Net();

	std::vector<_math::mat3> layer_3_filters;
	std::vector<_math::mat3> layer_1_filters;

	std::vector<_math::mat3> x;
};
