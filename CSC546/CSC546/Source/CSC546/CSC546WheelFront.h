// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "VehicleWheel.h"
#include "CSC546WheelFront.generated.h"

UCLASS()
class UCSC546WheelFront : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UCSC546WheelFront();
};



