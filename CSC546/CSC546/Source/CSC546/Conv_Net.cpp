// Fill out your copyright notice in the Description page of Project Settings.

#include "Conv_Net.h"
#define LEARN_RATE_CONV .00001
#define IMG_DEPTH 4

Conv_Net::Conv_Net()
{
	init();
}

Conv_Net::~Conv_Net()
{
}

std::vector<float> Conv_Net::fully_conn_mult(std::vector<float>input, std::vector<std::vector<float>>weights)
{
	std::vector<float> ret = std::vector<float>(input.size(), 0);
	for (int x = 0; x < input.size(); x++)
		for (int y = 0; y < weights[0].size(); y++)
			ret[x] = input[x] * weights[x][y];
	return ret;
}

_math::mat3 Conv_Net::forward_con(_math::mat3 input, std::vector<_math::mat3> filters, int step)
{
	int diameter = filters[0][0].size().y;
	_math::mat3 ret = _math::mat3(filters.size(), input.size().y / step, input.size().z / step);
	_math::mat3 temp = _math::mat3::z_fill(input, diameter - 1);
	for (int f = 0; f < filters.size(); f++)
		for (int y = diameter; y < temp.size().y - diameter; y += step)
			for (int z = diameter; z < temp[0][0].size() - diameter; z += step)
			{
				_math::mat3 sub = _math::mat3::sub_mat(temp, { 0, y, z }, { (int)temp.size().z, y + diameter - 1, z + diameter - 1 });
				ret[f][y - diameter][z - diameter] = _math::ReLU(_math::mat3::sum(filters[f] * sub));
			}
	return ret;
}

result2 Conv_Net::fully_con_back(std::vector<float> input, std::vector<std::vector<float>>& weights, std::vector<float>& error_above)
{
	std::vector<float> trans_mat = _math::apply_func(input, _math::ReLU, true), new_error;
	std::vector<std::vector<float>> change, new_weights;

	//change
	for (int i = 0; i < trans_mat.size(); i++)
	{
		std::vector<float>* temp = new std::vector<float>();
		for (int j = 0; j < error_above.size(); j++)
		{
			temp->push_back(trans_mat[i] * error_above[j]);
		}
		change.push_back(*temp);
		delete temp;
	}

	//new_weights
	for (int i = 0; i < change.size(); i++)
	{
		std::vector<float>* temp = new std::vector<float>();
		for (int j = 0; j < change[0].size(); j++)
		{
			temp->push_back(weights[j][i] + change[i][j] * LEARN_RATE_CONV);
		}
		new_weights.push_back(*temp);
		delete temp;
	}

	//new_error
	for (int i = 0; i < change[0].size(); i++)
	{
		std::vector<float>* temp = new std::vector<float>();
		for (int j = 0; j < change.size(); j++)
		{
			temp->push_back(weights[j][i] * change [i][j]);
		}
		new_error.push_back(_math::sum(*temp));
		delete temp;
	}

	std::vector<std::vector<float>> err_passer = std::vector<std::vector<float>>(1, new_error);

	return { new_weights, err_passer };
			
}

result4 Conv_Net::back_con(_math::mat3 input, std::vector<_math::mat3> filters, _math::mat3 final_error, int step)
{
	_math::mat3 lower_error = _math::mat3(input.size());
	_math::mat3 change, scaled_change, error;
	_math::mat3 new_filters[3] = { filters[0], filters[1], filters[2] };

	int diameter = filters[0][0].size().x;
	for (int f = 0; f < sizeof(filters) / sizeof(_math::mat3); f++)
		for (int y = 0; y < input.size().y; y += step)
			for (int z = 0; z < input[0][0].size(); z += step)
			{
				_math::mat3 sub = _math::mat3::sub_mat(input, { 0, (int)input.size().z, y }, { y + diameter - 1, z, z + diameter - 1 }).convert();
				change = (_math::mat3::apply_func(sub, _math::ReLU, true) * final_error[f][y / step][z / step]).convert();
				scaled_change = ((_math::mat3(change) * .01) * sub).convert();

				//this may break
				new_filters[f] = (_math::mat3(new_filters[f]) + _math::mat3(change)).convert();
				error = (filters[f] * _math::mat3(change)).convert();

				for (int ff = 0; ff < input.size().z; ff++)
					for (int dy = 0; dy < diameter; dy++)
						for (int dz = 0; dz < diameter; dz++)
							if ((y + dy) < lower_error[ff].size().y && z + dz < lower_error[ff].size().x)
								lower_error[ff][y + dy][z + dz] += error[ff][dy][dz];
			}
	return { new_filters, lower_error };
}

std::vector<float> Conv_Net::train(_math::mat3 img, int label)
{
	std::vector<float> final_err = std::vector<float>();
	std::vector<float> correct_output = std::vector<float>(4, 0);
	correct_output[label] = 1;

	//Forward
	_math::mat3 layer_1_output = forward_con(img, layer_1_filters, 1);
	result3 layer_2_output = forward_max_pool(layer_1_output, 2);
	_math::mat3 layer_3_output = forward_con(layer_2_output.y, layer_3_filters, 1);
	result3 layer_4_output = forward_max_pool(layer_3_output, 2);
	std::vector<float> final_output = layer_5->get_output(_math::mat3::_3d_1d(layer_4_output.y));

	for (int i = 0; i < final_output.size(); i++)
		final_err.push_back(final_output[i] - correct_output[i]);

	//backward
	final_err = layer_5->train(_math::mat3::_3d_1d(layer_4_output.x), correct_output);
	_math::mat3 layer_4_err = backwards_max_pool(layer_4_output.x, 2, _math::mat3::_1d_3d(final_err, { 4, 7, 7 }).convert());
	result4 layer_3_error = back_con(layer_2_output.y, layer_3_filters, layer_4_err, 1);

	for (int i = 0; i < sizeof(layer_3_error) / sizeof(_math::mat3); i++)
		layer_3_filters[i] = layer_3_error.x[i];

	_math::mat3 layer_2_error = backwards_max_pool(layer_2_output.x, 2, layer_3_error.y);
	result4 layer_1_error = back_con(img, layer_1_filters, layer_2_error, 1);

	for (int i = 0; i < sizeof(layer_3_error) / sizeof(_math::mat3); i++)
		layer_1_filters[i] = layer_1_error.x[i];

	return final_output;	
}

std::vector<float> Conv_Net::get_output(_math::mat3 img)
{
	//Forward
	_math::mat3 layer_1_output = forward_con(img, layer_1_filters, 1);
	result3 layer_2_output = forward_max_pool(layer_1_output, 2);
	_math::mat3 layer_3_output = forward_con(layer_2_output.y, layer_3_filters, 1);
	result3 layer_4_output = forward_max_pool(layer_3_output, 2);
	return layer_5->get_output(_math::mat3::_3d_1d(layer_4_output.y));
}

result3 Conv_Net::forward_max_pool(_math::mat3 input, int step)
{
	result3 ret;
	for (int i = 0; i < input.size().z; i++)
	{
		ret.x.push_back(_math::mat2::find_max_loc_2d(input[i], step));
		ret.y.push_back(_math::mat2::find_max_value_2d(input[i], step));
	}
	return ret;
}

_math::mat3 Conv_Net::backwards_max_pool(_math::mat3 loc, int step, _math::mat3 err)
{
	std::vector<_math::mat2> ret = loc.convert();
	for (int x = 0; x < ret.size(); x++)
		for (int y = 0; y < ret[0].size().y; y += step)
			for (int z = 0; z < ret[0].size().x; z += step)
			{
				int offset = loc[x][y / step][z / step];
				ret[x][y + offset][z + offset] = err[x][y / step][z / step];
			}
	return ret;
}

void Conv_Net::init()
{
	for (int i = 0; i < 3; i++)
	{
		layer_1_filters.push_back(_math::mat3::make_random_mat(1, 5, 5));
		layer_3_filters.push_back(_math::mat3::make_random_mat(4, 5, 5));
	}
	//this->layer_3_filters = std::vector<_math::mat3>(IMG_DEPTH, _math::mat3::make_random_mat(4, 5, 5));
}
