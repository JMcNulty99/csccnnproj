// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "CSC546GameMode.generated.h"

UCLASS(minimalapi)
class ACSC546GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACSC546GameMode();
};



