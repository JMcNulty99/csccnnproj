// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "CSC546WheelFront.h"

UCSC546WheelFront::UCSC546WheelFront()
{
	ShapeRadius = 35.f;
	ShapeWidth = 10.0f;
	bAffectedByHandbrake = false;
	SteerAngle = 50.f;
}
