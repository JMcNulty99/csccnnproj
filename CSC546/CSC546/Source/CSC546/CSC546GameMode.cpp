// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "CSC546GameMode.h"
#include "CSC546Pawn.h"
#include "CSC546Hud.h"

ACSC546GameMode::ACSC546GameMode()
{
	DefaultPawnClass = ACSC546Pawn::StaticClass();
	HUDClass = ACSC546Hud::StaticClass();
}
