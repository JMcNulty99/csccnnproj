// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Conv_Net.h"
#include "Runtime/Engine/Classes/Engine/TextureRenderTarget2D.h"
#include "Runtime/Engine/Classes/Engine/Texture2D.h"
#include "Runtime/Core/Public/Math/Color.h"
#include "GameFramework/Actor.h"
#include "Neural_Net.generated.h"

UCLASS()
class CSC546_API ANeural_Net : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ANeural_Net();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	Conv_Net CNN;

	template < typename t >
	TArray<t> vec_to_arr(std::vector<t> in);

	template < typename t >
	std::vector<t> arr_to_vec(TArray<t> in);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	TArray<float> train(TArray<float> input, TArray<float> output);
	
	UFUNCTION(BlueprintCallable, Category = "Neural Net")
		TArray<float> train(UTextureRenderTarget2D* input, TArray<float> output);

	UFUNCTION(BlueprintCallable, Category = "Neural Net")
		TArray<float> get_output(UTextureRenderTarget2D* input);
	

};