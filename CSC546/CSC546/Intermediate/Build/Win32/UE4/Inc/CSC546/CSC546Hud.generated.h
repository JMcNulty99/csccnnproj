// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CSC546_CSC546Hud_generated_h
#error "CSC546Hud.generated.h already included, missing '#pragma once' in CSC546Hud.h"
#endif
#define CSC546_CSC546Hud_generated_h

#define CSC546_Source_CSC546_CSC546Hud_h_10_RPC_WRAPPERS
#define CSC546_Source_CSC546_CSC546Hud_h_10_RPC_WRAPPERS_NO_PURE_DECLS
#define CSC546_Source_CSC546_CSC546Hud_h_10_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACSC546Hud(); \
	friend struct Z_Construct_UClass_ACSC546Hud_Statics; \
public: \
	DECLARE_CLASS(ACSC546Hud, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CSC546"), NO_API) \
	DECLARE_SERIALIZER(ACSC546Hud)


#define CSC546_Source_CSC546_CSC546Hud_h_10_INCLASS \
private: \
	static void StaticRegisterNativesACSC546Hud(); \
	friend struct Z_Construct_UClass_ACSC546Hud_Statics; \
public: \
	DECLARE_CLASS(ACSC546Hud, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CSC546"), NO_API) \
	DECLARE_SERIALIZER(ACSC546Hud)


#define CSC546_Source_CSC546_CSC546Hud_h_10_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACSC546Hud(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACSC546Hud) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACSC546Hud); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACSC546Hud); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACSC546Hud(ACSC546Hud&&); \
	NO_API ACSC546Hud(const ACSC546Hud&); \
public:


#define CSC546_Source_CSC546_CSC546Hud_h_10_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACSC546Hud(ACSC546Hud&&); \
	NO_API ACSC546Hud(const ACSC546Hud&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACSC546Hud); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACSC546Hud); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACSC546Hud)


#define CSC546_Source_CSC546_CSC546Hud_h_10_PRIVATE_PROPERTY_OFFSET
#define CSC546_Source_CSC546_CSC546Hud_h_7_PROLOG
#define CSC546_Source_CSC546_CSC546Hud_h_10_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CSC546_Source_CSC546_CSC546Hud_h_10_PRIVATE_PROPERTY_OFFSET \
	CSC546_Source_CSC546_CSC546Hud_h_10_RPC_WRAPPERS \
	CSC546_Source_CSC546_CSC546Hud_h_10_INCLASS \
	CSC546_Source_CSC546_CSC546Hud_h_10_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CSC546_Source_CSC546_CSC546Hud_h_10_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CSC546_Source_CSC546_CSC546Hud_h_10_PRIVATE_PROPERTY_OFFSET \
	CSC546_Source_CSC546_CSC546Hud_h_10_RPC_WRAPPERS_NO_PURE_DECLS \
	CSC546_Source_CSC546_CSC546Hud_h_10_INCLASS_NO_PURE_DECLS \
	CSC546_Source_CSC546_CSC546Hud_h_10_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CSC546_Source_CSC546_CSC546Hud_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
