// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CSC546/CSC546GameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCSC546GameMode() {}
// Cross Module References
	CSC546_API UClass* Z_Construct_UClass_ACSC546GameMode_NoRegister();
	CSC546_API UClass* Z_Construct_UClass_ACSC546GameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_CSC546();
// End Cross Module References
	void ACSC546GameMode::StaticRegisterNativesACSC546GameMode()
	{
	}
	UClass* Z_Construct_UClass_ACSC546GameMode_NoRegister()
	{
		return ACSC546GameMode::StaticClass();
	}
	struct Z_Construct_UClass_ACSC546GameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ACSC546GameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_CSC546,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACSC546GameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "CSC546GameMode.h" },
		{ "ModuleRelativePath", "CSC546GameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ACSC546GameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ACSC546GameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ACSC546GameMode_Statics::ClassParams = {
		&ACSC546GameMode::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x008802A8u,
		nullptr, 0,
		nullptr, 0,
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ACSC546GameMode_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ACSC546GameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ACSC546GameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ACSC546GameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACSC546GameMode, 609113255);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACSC546GameMode(Z_Construct_UClass_ACSC546GameMode, &ACSC546GameMode::StaticClass, TEXT("/Script/CSC546"), TEXT("ACSC546GameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACSC546GameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
