// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CSC546_CSC546WheelRear_generated_h
#error "CSC546WheelRear.generated.h already included, missing '#pragma once' in CSC546WheelRear.h"
#endif
#define CSC546_CSC546WheelRear_generated_h

#define CSC546_Source_CSC546_CSC546WheelRear_h_12_RPC_WRAPPERS
#define CSC546_Source_CSC546_CSC546WheelRear_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define CSC546_Source_CSC546_CSC546WheelRear_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCSC546WheelRear(); \
	friend struct Z_Construct_UClass_UCSC546WheelRear_Statics; \
public: \
	DECLARE_CLASS(UCSC546WheelRear, UVehicleWheel, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CSC546"), NO_API) \
	DECLARE_SERIALIZER(UCSC546WheelRear)


#define CSC546_Source_CSC546_CSC546WheelRear_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUCSC546WheelRear(); \
	friend struct Z_Construct_UClass_UCSC546WheelRear_Statics; \
public: \
	DECLARE_CLASS(UCSC546WheelRear, UVehicleWheel, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CSC546"), NO_API) \
	DECLARE_SERIALIZER(UCSC546WheelRear)


#define CSC546_Source_CSC546_CSC546WheelRear_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCSC546WheelRear(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCSC546WheelRear) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCSC546WheelRear); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCSC546WheelRear); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCSC546WheelRear(UCSC546WheelRear&&); \
	NO_API UCSC546WheelRear(const UCSC546WheelRear&); \
public:


#define CSC546_Source_CSC546_CSC546WheelRear_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCSC546WheelRear(UCSC546WheelRear&&); \
	NO_API UCSC546WheelRear(const UCSC546WheelRear&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCSC546WheelRear); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCSC546WheelRear); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCSC546WheelRear)


#define CSC546_Source_CSC546_CSC546WheelRear_h_12_PRIVATE_PROPERTY_OFFSET
#define CSC546_Source_CSC546_CSC546WheelRear_h_9_PROLOG
#define CSC546_Source_CSC546_CSC546WheelRear_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CSC546_Source_CSC546_CSC546WheelRear_h_12_PRIVATE_PROPERTY_OFFSET \
	CSC546_Source_CSC546_CSC546WheelRear_h_12_RPC_WRAPPERS \
	CSC546_Source_CSC546_CSC546WheelRear_h_12_INCLASS \
	CSC546_Source_CSC546_CSC546WheelRear_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CSC546_Source_CSC546_CSC546WheelRear_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CSC546_Source_CSC546_CSC546WheelRear_h_12_PRIVATE_PROPERTY_OFFSET \
	CSC546_Source_CSC546_CSC546WheelRear_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	CSC546_Source_CSC546_CSC546WheelRear_h_12_INCLASS_NO_PURE_DECLS \
	CSC546_Source_CSC546_CSC546WheelRear_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CSC546_Source_CSC546_CSC546WheelRear_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
