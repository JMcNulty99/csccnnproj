// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CSC546_CSC546Pawn_generated_h
#error "CSC546Pawn.generated.h already included, missing '#pragma once' in CSC546Pawn.h"
#endif
#define CSC546_CSC546Pawn_generated_h

#define CSC546_Source_CSC546_CSC546Pawn_h_17_RPC_WRAPPERS
#define CSC546_Source_CSC546_CSC546Pawn_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define CSC546_Source_CSC546_CSC546Pawn_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACSC546Pawn(); \
	friend struct Z_Construct_UClass_ACSC546Pawn_Statics; \
public: \
	DECLARE_CLASS(ACSC546Pawn, AWheeledVehicle, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CSC546"), NO_API) \
	DECLARE_SERIALIZER(ACSC546Pawn)


#define CSC546_Source_CSC546_CSC546Pawn_h_17_INCLASS \
private: \
	static void StaticRegisterNativesACSC546Pawn(); \
	friend struct Z_Construct_UClass_ACSC546Pawn_Statics; \
public: \
	DECLARE_CLASS(ACSC546Pawn, AWheeledVehicle, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CSC546"), NO_API) \
	DECLARE_SERIALIZER(ACSC546Pawn)


#define CSC546_Source_CSC546_CSC546Pawn_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACSC546Pawn(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACSC546Pawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACSC546Pawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACSC546Pawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACSC546Pawn(ACSC546Pawn&&); \
	NO_API ACSC546Pawn(const ACSC546Pawn&); \
public:


#define CSC546_Source_CSC546_CSC546Pawn_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACSC546Pawn(ACSC546Pawn&&); \
	NO_API ACSC546Pawn(const ACSC546Pawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACSC546Pawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACSC546Pawn); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACSC546Pawn)


#define CSC546_Source_CSC546_CSC546Pawn_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SpringArm() { return STRUCT_OFFSET(ACSC546Pawn, SpringArm); } \
	FORCEINLINE static uint32 __PPO__Camera() { return STRUCT_OFFSET(ACSC546Pawn, Camera); } \
	FORCEINLINE static uint32 __PPO__InternalCameraBase() { return STRUCT_OFFSET(ACSC546Pawn, InternalCameraBase); } \
	FORCEINLINE static uint32 __PPO__InternalCamera() { return STRUCT_OFFSET(ACSC546Pawn, InternalCamera); } \
	FORCEINLINE static uint32 __PPO__InCarSpeed() { return STRUCT_OFFSET(ACSC546Pawn, InCarSpeed); } \
	FORCEINLINE static uint32 __PPO__InCarGear() { return STRUCT_OFFSET(ACSC546Pawn, InCarGear); }


#define CSC546_Source_CSC546_CSC546Pawn_h_14_PROLOG
#define CSC546_Source_CSC546_CSC546Pawn_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CSC546_Source_CSC546_CSC546Pawn_h_17_PRIVATE_PROPERTY_OFFSET \
	CSC546_Source_CSC546_CSC546Pawn_h_17_RPC_WRAPPERS \
	CSC546_Source_CSC546_CSC546Pawn_h_17_INCLASS \
	CSC546_Source_CSC546_CSC546Pawn_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CSC546_Source_CSC546_CSC546Pawn_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CSC546_Source_CSC546_CSC546Pawn_h_17_PRIVATE_PROPERTY_OFFSET \
	CSC546_Source_CSC546_CSC546Pawn_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	CSC546_Source_CSC546_CSC546Pawn_h_17_INCLASS_NO_PURE_DECLS \
	CSC546_Source_CSC546_CSC546Pawn_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CSC546_Source_CSC546_CSC546Pawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
