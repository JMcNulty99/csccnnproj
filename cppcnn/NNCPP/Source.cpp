#include <vector>						//dynamic arrays
#include <iostream>						//IO
#include "neural_network/neural_net.h"	//neural network

using namespace std;
using namespace _math;
using namespace _nn;
//using namespace _cnn;

float square(float);

int main()
{
	/*mat2 y = mat2::make_random_mat(2, 2);
	mat2 z = {2, 2, 3};
	cout << y * z << endl;
	cout << mat2::apply_func(y*z, sigmoid);*/
	
	neural_net x({ 3, 2, 2, 1 });
	cout << x;
	cin.get();
	return 0;
}

float square(float x)
{
	return x * x;
}