#pragma once
#include "mat2.h"
namespace _math
{
	struct index3 : index2													//represents a 3d location
	{
		index3(int _x = 0, int _y = 0, int _z = 0) : index2(_x, _y)
		{
			z = _z;
		}
		index3(index2 _xy, int _z) : index2(_xy)
		{
			z = _z;
		}
		index3(int _x) : index2(_x, _x)
		{
			z = _x;
		}
		int z;
	};

	class mat3
	{
	public:
		mat3(int x = 1, int y = 1, int z = 1, float f = 0);
		mat3(index3 x, float f = 0);
		~mat3();

		static float sum(mat3);														//finds the sum of each element in the matrix
		static float find_max_value(mat3);											//find the value of the max number in the mat3

		static std::vector < float> _3d_1d(mat3);									//returns a 1d array from a 3d matrix
		static mat3 _1d_3d(std::vector<float>, index3);								//creates a mat3 from 1d array

		static index3 find_max_loc(mat3);											//find the index of the max number in the mat3
		index3 size();																//returns an index container of the dimensions

		static mat3 z_fill(mat3, int);												//creates a matrix that is padded with n zeroes
		void z_fill(int);															//creates a matrix that is padded with n zeroes
		
		static mat3 make_random_mat(int x = 1, int y = 1, int z = 1);				//creates a new matrix of size {x, y} with random values
		static mat3 sub_mat(mat3, int, int, int, int, int, int);					//create a submatrix from the range provided (inclusive)
		static mat3 sub_mat(mat3, index3, index3);									//create a submatrix from the range provided (inclusive)
		static mat3 apply_func(mat3, float(*f)(float, bool), bool deriv = false);	//applies the function specified to the matrix
		void apply_func( float(*f)(float, bool), bool deriv = false);				//applies the function specified to the matrix

		void push_back(mat2);														//appends a mat2 into the mat3, extending the z dimension by 1
		void remove_first();														//removes the first mat2 from the mat3
		mat2 pop_back();															//removes the highest z mat2 from the mat3, decreasing dim(z) by 1


		mat2& operator[](int);														//accessor for the wrapped array
		float& operator[](index3);

		friend mat3 operator*(mat3, mat3);											//elemental multiplication of matracies
		friend mat3 operator*(mat3, float);											//scalar multiplication of matracies
		friend std::ostream& operator<<(std::ostream&, mat3);						//cout

	private:
		std::vector< mat2 > elem;													//matrix array
		index3 _size;
	};
}

