#pragma once
#include <iostream>
#include <vector>
#include <random>

namespace _math
{
	struct index2																	//represents a 2d location
	{
		index2(int _x = 0, int _y = 0)
		{
			x = _x;
			y = _y;
		}
		index2(int _x)
		{
			x = _x;
			y = _x;
		}
		int x, y;
	};

	class mat2
	{
	public:
		mat2(int x = 1, int y = 1, float f = 0);
		mat2(index2 x, float f = 0);
		~mat2();
		
		//float
		static float sum(mat2);														//finds the sum of each element in the matrix
		static float find_max_value(mat2);											//find the value of the max number in the mat2

		//index
		static index2 find_max_loc(mat2);											//find the index of the max number in the mat2
		index2 size();																//returns an index container of the dimensions
		
		//mat2
		static mat2 z_fill(mat2, int);												//creates a matrix that is padded with n zeroes
		void z_fill(int);															//creates a matrix that is padded with n zeroes
		static mat2 make_random_mat(int x, int y);									//creates a new matrix of size {x, y} with random values
		static mat2 sub_mat(mat2, int, int, int, int);								//create a submatrix from the range provided (inclusive)
		static mat2 sub_mat(mat2, index2, index2);									//create a submatrix from the range provided (inclusive)
		static mat2 apply_func(mat2, float(*f)(float, bool), bool deriv = false);	//applies the function specified to the matrix
		void apply_func(float(*f)(float, bool), bool deriv = false);				//applies the function specified to the matrix



		//std::vector<float>
		std::vector<float>& operator[](int);										//accessor for the wrapped array
		float& operator[](index2);

		//friend
		friend mat2 operator+(mat2, mat2);											//matrix addition
		friend mat2 operator*(mat2, mat2);											//elemental multiplication of matracies
		friend mat2 operator*(mat2, float);											//scalar multiplication of matracies
		friend std::ostream& operator<<(std::ostream&, mat2);						//cout

	private:
		std::vector< std::vector< float > > elem;									//matrix array
		index2 _size;
	};
}

