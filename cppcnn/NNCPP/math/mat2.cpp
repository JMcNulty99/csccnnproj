#include "mat2.h"


namespace _math
{
	mat2::mat2(int x, int y, float f)
	{
		//initialize all of the values of the vector array for the matrix
		elem = std::vector< std::vector<float> >(y, std::vector<float>(x, f));
		_size = { x, y };
	}

	mat2::mat2(index2 x, float f)
	{
		//initialize all of the values of the vector array for the matrix
		elem = std::vector< std::vector<float> >(x.y, std::vector<float>(x.x, f));
		_size = x;
	}

	mat2::~mat2()
	{
	}

	float mat2::sum(mat2 _mat)
	{
		float sum = 0;
		for (std::vector<float> i : _mat.elem)	//for each vector in elem
			for (float x : i)					//for each float in vector
				sum += x;
		return sum;
	}

	index2 mat2::find_max_loc(mat2 _mat)
	{
		index2 loc = { 0,0 };
		float max = 0;
		for (int i = 0; i < _mat.size().y; i++)
			for (int j = 0; j < _mat.size().x; j++)
				if (_mat[i][j] > max)
					loc = { i, j };
		return loc;
	}

	float mat2::find_max_value(mat2 _mat)
	{
		float max = 0;
		for (int i = 0; i < _mat.size().y; i++)
			for (int j = 0; j < _mat.size().x; j++)
				if (_mat[i][j] > max)
					max = _mat[{i, j}];
		return max;
	}

	index2 mat2::size()
	{
		return _size;
	}

	void mat2::z_fill(int x)
	{
		mat2 res = mat2({ this->size().x + 2 * x });
		for (int i = 0; i < this->size().y; i++)
			for (int j = 0; j < this->size().y; j++)
				res[i + x][j + x] = (*this)[i][j];
		(*this) = res;
	}

	mat2 mat2::z_fill(mat2 _mat, int x)
	{
		mat2 res = mat2({ _mat.size().x + 2 * x });
		for (int i = 0; i < _mat.size().y; i++)
			for (int j = 0; j < _mat.size().y; j++)
				res[i + x][j + x] = _mat[i][j];
		return res;
	}

	mat2 mat2::make_random_mat(int x, int y)
	{
		//setup random generator
		std::default_random_engine rand;
		std::uniform_real_distribution<float> dist(-5, 5);

		//create new matrix with random values
		mat2 res = mat2(x, y);
		for (int i = 0; i < y; i++)
			for (int j = 0; j < x; j++)
				res[{i, j}] = dist(rand);
		return res;
	}

	mat2 mat2::sub_mat(mat2 _mat, int x1, int y1, int x2, int y2)
	{
		mat2 res = mat2(x2 - x1 + 1, y2 - y1 + 1);
			for (int j = 0; j < y2 - y1 + 1; j++)
				for (int k = 0; k < x2 - x1 + 1; k++)
					if (j + y1 < _mat.size().y && k + x1 < _mat.size().x)
						res[j][k] = _mat[j + y1][k + x1];
					else
						res[j][k] = 0;
		return res;
	}

	mat2 mat2::sub_mat(mat2 _mat, index2 start, index2 end)
	{
		mat2 res = mat2(end.x - start.x + 1, end.y - start.x + 1);
		for (int j = 0; j < end.y - start.x + 1; j++)
			for (int k = 0; k < end.x - start.x + 1; k++)
				if (j + start.x < _mat.size().y && k + start.x < _mat.size().x)
					res[j][k] = _mat[j + start.x][k + start.x];
				else
					res[j][k] = 0;
		return res;
	}

	mat2 mat2::apply_func(mat2 _mat, float(*f)(float, bool), bool deriv)
	{
		mat2 res = mat2(_mat.size());
		for (int j = 0; j < _mat.size().y; j++)
			for (int k = 0; k < _mat.size().x; k++)
				res[j][k] = f(_mat[j][k], deriv);
		return res;
	}

	void mat2::apply_func(float(*f)(float, bool), bool deriv)
	{
		mat2 res = mat2(size());
		for (int j = 0; j < size().y; j++)
			for (int k = 0; k < size().x; k++)
				res[j][k] = f((*this)[j][k], deriv);
		(*this) = res;
	}

	std::vector<float>& mat2::operator[](int x)
	{
		return elem[x];
	}

	float& mat2::operator[](index2 x)
	{
		return elem[x.y][x.x];
	}

	mat2 operator+(mat2 a, mat2 b)
	{
		mat2 res = mat2(a.size());
		for (int i = 0; i < a.size().y; i++)
			for (int j = 0; j < a.size().x; j++)
				res[i][j] = a[i][j] + b[i][j];
		return res;
	}

	mat2 operator*(mat2 a, mat2 b)
	{
		mat2 res = mat2(a.size());
		for (int i = 0; i < a.size().y; i++)
			for (int j = 0; j < a.size().x; j++)
				res[i][j] = a[i][j] * b[i][j];
		return res;
	}

	mat2 operator*(mat2 a, float s)
	{
		mat2 res = mat2(a.size());
		for (int i = 0; i < a.size().y; i++)
			for (int j = 0; j < a.size().x; j++)
				res[i][j] = a[i][j] * s;
		return res;
	}

	std::ostream& operator<<(std::ostream& os, mat2 x)
	{
		for (int i = 0; i < x.size().y; i++)
		{
			os << "| ";
			for (int j = 0; j < x.size().x; j++)
				os << x[i][j] << ", ";
			os << "\b\b |\n";
		}
		return os;
	}
}