#include "neural_net.h"
namespace _nn
{
	neural_net::neural_net(std::vector<int> layers, bool relu, std::string name)
	{

		this->layer_count = layers.size();
		this->relu = relu;
		this->name = name;
		last_error = _math::mat3();
		//init data from file
		try
		{
			//this->read()
			throw this;
		}
		catch(void*)
		{
			for (int i = this->layer_count - 1; i >= 0; i--)
				weights.push_back(_math::mat2::make_random_mat(layers[i], layers[i]));
			weights.remove_first();
		}
	}


	neural_net::~neural_net()
	{
	}

	std::ostream & operator<<(std::ostream & os, neural_net x)
	{
		os << x.weights;
		return os;
	}
}