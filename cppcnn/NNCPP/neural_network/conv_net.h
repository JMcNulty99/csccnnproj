#pragma once

#include "neural_net.h"

namespace _cnn
{
	class conv_net
	{
	public:
		conv_net();
		~conv_net();

		//std::vector<float>
		std::vector<float> fully_con_mult(std::vector<float>, _math::mat2);

		void fully_con_back(std::vector<float>, _math::mat2, std::vector<float>);
		void back_con(_math::mat2, _math::mat2&, _math::mat2&, int);

		_math::mat2 forward_con(_math::mat2, std::vector<_math::mat2>, int);
		_math::mat2 forward_con(_math::mat2, _math::mat3, int);
		
		
	};
}