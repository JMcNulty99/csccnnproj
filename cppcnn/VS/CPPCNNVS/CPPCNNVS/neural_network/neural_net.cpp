#include "neural_net.h"

#define LEARNING_RATE .5

namespace _nn
{
	neural_net::neural_net(std::vector<int> numlayers, bool relu, std::string name)
	{

		this->layer_count = (int)numlayers.size();
		this->relu = relu;
		this->name = name;
		this->output_size = numlayers.back();
		last_error = std::vector<float>();


		//init data from file
		try
		{
			//this->read()
			throw this;
		}
		catch(void*)
		{
			std::vector<std::vector<float>> _layers_temp = std::vector<std::vector<float>>();
			for (int i = 0; i < this->layer_count; i++)
			{
				std::vector<float> temp = std::vector<float>();
				for (int j = 0; j < numlayers[i]; j++)
					temp.push_back(0);
				this->layers.push_back(temp);
			}

			std::cout << _math::mat2(layers) << std::endl;

			for (int i = 0; i < layer_count - 1; i++)
				this->weights.push_back(_math::mat2::make_random_mat(numlayers[i+1], numlayers[i]).convert());
		}
	}

	neural_net::~neural_net()
	{
	}

	_math::mat2 neural_net::dot_with_func(_math::mat2 x, _math::mat2 y, float(*f)(float, bool), bool deriv)
	{
		return _math::mat2::apply_func(x.mult(y)._t(), f, deriv);
	}

	std::vector<float> neural_net::dot_with_func(std::vector<float> x, _math::mat2 y, float(*f)(float, bool), bool deriv)
	{
		std::vector<float> ret = std::vector<float>();
		for (int c = 0; c < y.size().x; c++) // <<< eh? :D
		{
			float sum = 0;
			for (int r = 0; r < y.size().y; r++)
				sum += x[r] * y[r][c];
			ret.push_back(f(sum, deriv));
		}
		return ret;
	}

	std::vector<float> neural_net::train(std::vector<float> input_data, std::vector<float> output_data)
	{
		std::vector<float> ret = get_output(input_data);
		std::vector<float> err = std::vector<float>(output_data.size());
		for (int i = 0; i < err.size(); i++)
			err[i] = (output_data[i] - ret[i]);
		backpropogate(err);
		return err;
	}

	void neural_net::backpropogate(std::vector<float> err)
	{
		this->last_error = err;

		std::vector< std::vector<float> > error = std::vector< std::vector<float> >(this->layers.size(), std::vector<float>());
		std::vector< std::vector<float> > delta = std::vector< std::vector<float> >(this->layers.size(), std::vector<float>());
		
		//line97
		error.back() = err;
		
		//line98
		for (int i = 0; i < error.back().size(); i++)
			delta.back().push_back(error.back()[i] * _math::sigmoid(layers.back()[i], true));

		//total guess line 106
		for (int i = this->layers.size() - 2; i > -1; i--)
		{
			//1 temp is new
			std::vector< std::vector<float> > temp = std::vector<std::vector<float>>(this->weights[i][0].size(), std::vector<float>(this->weights[i].size(), 0));
			
			//2
			for (int r = 0; r < this->weights[i].size(); r++)
				for (int c = 0; c < this->weights[i][0].size(); c++)
					temp[c][r] = this->weights[i][r][c];
	
			//3
			error[i] = dot_with_func(delta[i + 1], temp, _math::x);
			
			for (int j = 0; j < error[i].size(); j++)
				delta[i].push_back(error[i][j] * _math::sigmoid(this->layers[i][j], true));
		}

		//4
		for (int i = 0; i < this->weights.size(); i++)
			for (int r = 0; r < this->weights[i].size(); r++)
				for (int c = 0; c < this->weights[i][0].size(); c++)
				{
					float y = this->layers[i][r];
					float x = y * delta[i + 1][c] * LEARNING_RATE;
					this->weights[i][r][c] += x;
				}
	}

	std::vector<float> neural_net::get_output(std::vector<float> data)
	{
		if (data.size() != this->layers[0].size())
			throw "Layer Dim Mismatch";

		this->layers[0] = data;
		
		for (int i = 1; i < this->layer_count - 1; i++)
			this->layers[i] = dot_with_func(this->layers[i - 1], this->weights[i - 1], _math::sigmoid);

		this->layers.back() = dot_with_func(layers[layers.size() - 2], weights.back(), _math::sigmoid);
		return this->layers.back();
	}

	int neural_net::size()
	{
		return this->output_size;
	}

	std::ostream & operator<<(std::ostream & os, neural_net x)
	{
		return os <<_math::mat2(x.layers);
	}
}