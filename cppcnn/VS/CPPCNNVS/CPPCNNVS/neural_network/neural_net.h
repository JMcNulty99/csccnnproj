#pragma once
#include <vector>
#include <math.h>

#include "../math/mat3.h"
#include "../math/_math.h"

namespace _nn
{
	class neural_net
	{
	public:
		neural_net(std::vector<int>, bool ReLU = false, std::string name = "default_name");
		~neural_net();

		//_math::mat3
		static _math::mat2 dot_with_func(_math::mat2 x, _math::mat2 y, float(*f)(float, bool), bool deriv = false);						//applies a function to the dot product of two matricies
		static std::vector<float> dot_with_func(std::vector<float> x, _math::mat2 y, float(*f)(float, bool), bool deriv = false);		//applies a function to the dot product of a layer and a matrix
		std::vector<float> train(std::vector<float>, std::vector<float>);
		void backpropogate(std::vector<float>);

		//std::vector<float>
		std::vector<float> get_output(std::vector<float>);

		//int
		int size();

		//void
		void read_data();
		void learn_data();

		void learn_from_file(std::string);

		friend std::ostream& operator<<(std::ostream&, neural_net);

	private:
		//bool
		bool relu;
		
		//std::string
		std::string name;
		
		//int
		int layer_count;
		int output_size;

		//std::vector< std::vector<float> >
		std::vector<std::vector<float>> layers;

		//mat3
		std::vector<float> last_error;
		std::vector<std::vector<std::vector<float>>> weights;

	};
}