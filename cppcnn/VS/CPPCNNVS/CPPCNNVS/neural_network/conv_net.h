#pragma once

#include "neural_net.h"

namespace _cnn
{
	class conv_net
	{
	public:
		conv_net();
		~conv_net();

		//std::vector<float>
		std::vector<std::vector<float>> fully_conn_mult(std::vector<float>, std::vector<std::vector<float>>);
		std::vector<std::vector<float>> forward_con(std::vector<std::vector<float>>, std::vector<std::vector<std::vector<float>>>, int);
		void fully_con_back(std::vector<float>, std::vector<std::vector<float>>, std::vector<float>);
		void back_con(std::vector<std::vector<float>>, std::vector<std::vector<float>>&, std::vector<std::vector<float>>&, int);



	};
}